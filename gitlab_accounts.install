<?php

/**
 * @file
 * Install, update, and uninstall functions for GitLab Accounts.
 */

/**
 * Implements hook_schema().
 */
function gitlab_accounts_schema() {
  $schema = array();
  // Table for GitLab account entities.
  $schema['gitlab_accounts'] = array(
    'description' => 'Base table for GitLab account entities.',
    'fields' => array(
      'id' => array(
        'description' => 'Primary key of the account.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'remote_id' => array(
        'description' => 'The remote id of the account.',
        'type' => 'int',
        'size' => 'normal',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'The name of the account.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'username' => array(
        'description' => 'The username of the account.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'endpoint_url' => array(
        'description' => 'The endpoint URL used to connect to the account.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'private_token' => array(
        'description' => 'The private token of the account.',
        'type' => 'blob',
        'size' => 'normal',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
    'indexes' => array(
      'gitlab_account_username' => array('id', 'username'),
    ),
  );

  return $schema;
}
